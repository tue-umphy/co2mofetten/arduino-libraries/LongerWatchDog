#include <LongerWatchDog.h>

#define SLEEP_MS 11000
#define RESET_MS 91136

LongerWatchDog WatchDog;

void setup()
{
  WatchDog.disable();
  Serial.begin(9600);
  while(not Serial);
  Serial.println("Setup: finished");Serial.flush();
}

void loop()
{
  Serial.print("Loop: tell to sleep [ms] ");
  Serial.println(SLEEP_MS);Serial.flush();
  WatchDog.sleep_ms(SLEEP_MS);
  Serial.println("Loop: woke up");Serial.flush();
  Serial.print("Loop: tell to reset after [ms] ");
  Serial.println(RESET_MS);Serial.flush();
  WatchDog.device_reset_after_ms(RESET_MS);
  Serial.println("Loop: entering endless loop...");Serial.flush();
  while(true){
    Serial.println("Loop: Looping uselessly...");Serial.flush();
    delay(500);
    }
}

