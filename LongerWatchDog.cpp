/*
  LongerWatchDog.cpp - Library for longer watchdog timers
  Created by Yann Büchau, April 19, 2018.

  This is an adjusted version of the Adafruit_SleepyDog library
*/

#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include "LongerWatchDog.h"
#include "Arduino.h"
#include <avr/wdt.h>

#define TIMEOUT_THRESHOLD_MS 150

#ifdef LongerWatchDog_DEBUG
  #define LONGERWATCHDOG_DEBUG_PREFIX "LongerWatchDog: "
  #define DF(s) Serial.print(F(s))
  #define DN(s) Serial.print(s)
  #define DP(cmd) {DF(LONGERWATCHDOG_DEBUG_PREFIX);cmd;\
    Serial.println();Serial.flush();}
  #define DS(s) DP(DF(s))
  #define DSN(s,n) DP(DF(s);DN(n))
  #ifdef UDR0
    // in ISR, one should not use Serial.print, but it's possible to write
    // single bytes by writing to UDR0
    // https://forum.arduino.cc/index.php?topic=86485.0
    #define DISRUDR0(chr) {while(!UCSR0A & _BV(UDRE0));UDR0=chr;}
  #else
    #define DISRUDR0(chr)
  #endif
  #ifdef LongerWatchDog_ISR_SERIAL
    #undef DISRUDR0
    #define DISRUDR0(chr) // if Serial.print is used in ISR, no need for UDR0
    #define DISR(cmd) {cmd}
  #else
    #define DISR(cmd)
  #endif
#else
  #define DF(s)
  #define DN(s)
  #define DP(cmd)
  #define DS(s)
  #define DSN(s,n)
  #define DISRUDR0(cmd)
  #define DISR(cmd)
#endif

volatile long lwd_total_timeout_ms = 0;
volatile long lwd_total_time_ms = 0;
volatile long lwd_remaining_time_ms = 0;
volatile int lwd_current_timeout_ms = 0;
volatile int lwd_current_timeout = WDTO_15MS;
volatile bool sleeping = false;

void LongerWatchDog::device_reset_after_ms(long timeout_ms) {
  DSN("Will reset the device after [ms] ",timeout_ms);
  #ifndef LongerWatchDog_ISR_SERIAL
  DS("('|' = ISR entered, '!' = reset)");
  #endif
  lwd_total_time_ms = 0;
  lwd_total_timeout_ms=timeout_ms;
  longest_timout_below(timeout_ms, lwd_current_timeout, lwd_current_timeout_ms);
  interrupt_after(lwd_current_timeout);
  lwd_total_time_ms += lwd_current_timeout_ms;
  sleeping = false;
}

int LongerWatchDog::prescaler_register(int wdto) {
  uint8_t wdps;
  wdps = ((wdto & 0x08 ? 1 : 0) << WDP3) |
         ((wdto & 0x04 ? 1 : 0) << WDP2) |
         ((wdto & 0x02 ? 1 : 0) << WDP1) |
         ((wdto & 0x01 ? 1 : 0) << WDP0);
  return wdps;
  }

void LongerWatchDog::interrupt_after(int wdto) {
    // Build watchdog prescaler register value before timing critical code.
    uint8_t wdps = prescaler_register(wdto);
    // The next section is timing critical so interrupts are disabled.
    cli();
    // First clear any previous watchdog reset.
    MCUSR &= ~ _BV(WDRF);
    // Now change the watchdog prescaler and interrupt enable bit so the
    // watchdog reset only triggers the interrupt (and wakes from deep sleep)
    // and not a full device reset.  This is a timing critical section of code
    // that must happen in 4 cycles.
    WDTCSR |= _BV(WDCE) | _BV(WDE);  // Set WDCE and WDE to enable changes.
    WDTCSR = wdps;                   // Set the prescaler bit values.
    WDTCSR |= _BV(WDIE);             // Enable only watchdog interrupts.
    // Critical section finished, re-enable interrupts.
    sei();
    }

void LongerWatchDog::device_reset_after_wdto(int wdto) {
    uint8_t wdps = prescaler_register(wdto);
    cli();
    MCUSR &= ~ _BV(WDRF);
    WDTCSR |= _BV(WDCE) | _BV(WDE);  // Set WDCE and WDE to enable changes.
    WDTCSR = wdps;                   // Set the prescaler bit values.
    WDTCSR |= _BV(WDE);             // Enable only watchdog interrupts.
    sei();
    }

void LongerWatchDog::put_to_sleep() {
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleeping = true;
  sleep_mode();
  }

void LongerWatchDog::sleep_ms(long sleep_ms) {
  // Disable USB if it exists
  #ifdef USBCON
    USBCON |= _BV(FRZCLK);  //freeze USB clock
    PLLCSR &= ~_BV(PLLE);   // turn off USB PLL
    USBCON &= ~_BV(USBE);   // disable USB
  #endif

  DSN("Will put the device to sleep for [ms] ",sleep_ms);
  #ifndef LongerWatchDog_ISR_SERIAL
  DS("Won't show ISR messages without LongerWatchDog_ISR_SERIAL defined");
  #endif
  lwd_total_time_ms = 0; // reset timer
  lwd_total_timeout_ms = sleep_ms; // reset timer
  longest_timout_below(sleep_ms, lwd_current_timeout, lwd_current_timeout_ms);
  interrupt_after(lwd_current_timeout);
  lwd_total_time_ms += lwd_current_timeout_ms;
  put_to_sleep();
  }

ISR(WDT_vect) {
  lwd_remaining_time_ms = lwd_total_timeout_ms - lwd_total_time_ms;
  if(!sleeping) DISRUDR0(124); // 124 = | // does not work in sleep mode...
  DISR(DSN("ISR: Remaining time: ",lwd_remaining_time_ms));
  if (lwd_remaining_time_ms > TIMEOUT_THRESHOLD_MS) {
    LongerWatchDog::longest_timout_below
      (lwd_remaining_time_ms, lwd_current_timeout, lwd_current_timeout_ms);
    LongerWatchDog::interrupt_after(lwd_current_timeout);
    lwd_total_time_ms += lwd_current_timeout_ms;
    if(sleeping) {
      // DISRUDR0(126); // 126 = ~ // does not work in sleep mode...
      DISR(DS("ISR: Putting back to sleep mode"));
      LongerWatchDog::put_to_sleep();
    }
  }
  else {
    DISR(DSN("ISR: Timeout reached within tolerance of [ms] ",
      TIMEOUT_THRESHOLD_MS));
    if(sleeping) {
      // DISRUDR0(42); // 42 = * // does not work in sleep mode...
      DISR(DS("ISR: Disabling sleep"));
      sleeping=false;
      sleep_disable(); // stop sleeping
      LongerWatchDog::disable(); // disabling watchdog
    }
    else {
      DISRUDR0(33); // 33 = !
      DISR(DS("ISR: Resetting!"));
      LongerWatchDog::device_reset(); // reset now
    }
  }
}

void LongerWatchDog::device_reset() {
 device_reset_after_wdto(WDTO_15MS); // reset as quick as you can
 while(1); // block forever until the reset kicks in
 }

void LongerWatchDog::reset() { wdt_reset(); }

void LongerWatchDog::disable() { wdt_disable(); }

void LongerWatchDog::longest_timout_below
  (volatile long maxMS, volatile int &wdto, volatile int &actualMS) {
  int MS_vals [] = {8000,4000,2000,1000,500,250,120,60,30,15};
  int WD_vals [] = {WDTO_8S,WDTO_4S,WDTO_2S,WDTO_1S,WDTO_500MS,WDTO_250MS,
    WDTO_120MS,WDTO_60MS,WDTO_30MS,WDTO_15MS};

  // search for the first element smaller than the requested time
  for ( unsigned int i = 0; i < 9; i++ ) {
    if (maxMS >= MS_vals[i]) {
        wdto     = WD_vals[i];
        actualMS = MS_vals[i];
        return; // stop searching
      }
    }
  // if nothing matched, use the smallest time
  wdto     = WD_vals[9];
  actualMS = MS_vals[9];
}

